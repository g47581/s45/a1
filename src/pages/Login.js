import { Container, Row, Col, Form, Button} from 'react-bootstrap'
import {useState, useEffect} from 'react'
import {useNavigate} from 'react-router-dom'

export default function Login(){
	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)
	const navigate = useNavigate()
	const loginUser = (e) => {
		e.preventDefault()

		fetch('http://localhost:3007/api/users/login', {
			method:"POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				localStorage.setItem('token', response.token)
				localStorage.setItem('email', email)
				// console.log(localStorage.getItem(email))
				alert('Login Successfully!')

				setEmail("")
				setPW("")
				

				navigate('/courses')
			}else{
				alert('Incorrect credentials!')
			}

			
		})


	}

	useEffect(() => {
		// if all fields are filled out and pw & vpw is equal, change the state to false
		if(email !== "" && pw !== "" ) {

			setIsDisabled(false)

		} else {
			//if all input fields are empty, keep the state of the button to true
			setIsDisabled(true)
		}

		//listen to state changes: fn, ln, em, pw, vf
	}, [email, pw])


	return (
		<Container>
			<h3 className="text-center">Login</h3>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => loginUser(e) }>
						<Form.Group className="mb-3">
							<Form.Label>Email address</Form.Label>
					    	<Form.Control 
					    		type="email" 
					    		value={email}
					    		onChange={(e) => setEmail(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
					    	<Form.Label>Password</Form.Label>
					    	<Form.Control 
					    		type="password" 
					    		value={pw}
					    		onChange={(e) => setPW(e.target.value)}
					    	/>
						</Form.Group>
						<Button 
							variant="info" 
							type="submit"
							disabled={isDisabled}
						>
							Login
						</Button>
					</Form>
					
				</Col>
			</Row>


		</Container>

		)



}